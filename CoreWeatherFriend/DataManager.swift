//
//  DataManager.swift
//  Weather Friend
//
//  Created by David Velarde on 7/11/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import Foundation

public class DataManager: NSObject {
	public static let shared = DataManager()
	
	public func setUnitSystem(newSystem: UnitSystem) {
		UserSettings.currentSystem = newSystem
	}
	
	public func save(newLocation: BMLocation) -> Bool {
		let exists = Store.shared.savedLocations.filter { (location) -> Bool in
			location.locationName == newLocation.locationName
		}.count > 0
		if !exists {
			Store.shared.savedLocations.append(newLocation)
			return true
		} else {
			return false
		}
	}
	public func getBMLocations() -> [BMLocation] {
		return Store.shared.savedLocations
	}
	public func deleteLocation(index: Int) {
		Store.shared.savedLocations.remove(at: index)
	}
	public func deleteAllLocations() {
		Store.shared.savedLocations.removeAll()
	}
}
