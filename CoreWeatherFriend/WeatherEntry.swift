//
//  WeatherEntry.swift
//  Weather Friend
//
//  Created by David Velarde on 7/11/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

public struct WeatherInfo {
	public let code: Int
	public let state: String
	public let isDay: Bool
	public let type: WeatherType
	
	init(json: JSON) {
		code = json["id"].intValue
		state = json["description"].stringValue
		isDay = json["icon"].stringValue.contains("d")
		type = WeatherType.byCode(code, isDay: isDay)
	}
}

public struct WeatherEntry {
	
	public let entryDate: Date
	public let temperature: Temperature
	public var minTemp: Temperature
	public var maxTemp: Temperature
	public var placeName: String
	public let info: WeatherInfo
	public let pressure: Double
	public let humidity: Double
	public let snow: Double
	public let rain: Double
	public let wind: String
	
	init?(json: JSON) {
		let timeStamp = json["dt"].doubleValue
		entryDate = Date(timeIntervalSince1970: timeStamp)
		let tempValue = json["main"]["temp"].doubleValue
		temperature = Temperature(tempValue)
		
		let minTempValue = json["main"]["temp_min"].doubleValue
		let maxTempValue = json["main"]["temp_max"].doubleValue
		
		minTemp = Temperature(minTempValue)
		maxTemp = Temperature(maxTempValue)
		
		placeName = json["name"].stringValue
		
		let tempWeather = json["weather"].arrayValue.first!
		var weatherInfo: JSON?
		switch tempWeather {
		case .dictionary(let value):
			weatherInfo = value
		default:
			break
		}
		if let weatherInfo = weatherInfo {
			info = WeatherInfo(json: weatherInfo)
		} else {
			return nil
		}
		pressure = json["main"]["pressure"].doubleValue
		humidity = json["main"]["humidity"].doubleValue
		
		if json["rain"].isNull || json["rain"]["3h"].isNull {
			rain = 0
		} else {
			rain = json["rain"]["3h"].doubleValue
		}
		
		if json["snow"].isNull || json["snow"]["3h"].isNull {
			snow = 0
		} else {
			snow = json["snow"]["3h"].doubleValue
		}
		
		let windSpeed = json["wind"]["speed"].doubleValue
		let windDirection = json["wind"]["deg"].doubleValue
		
		var windDirectionNotation = "N"
		
		if windDirection == 0 {
			windDirectionNotation = "N"
		} else if windDirection.isBetween(lower: 0, upper: 45, including: false) {
			windDirectionNotation = "NNE"
		} else if windDirection == 45 {
			windDirectionNotation = "NE"
		} else if windDirection.isBetween(lower: 45, upper: 90, including: false) {
			windDirectionNotation = "ENE"
		} else if windDirection == 90 {
			windDirectionNotation = "E"
		} else if windDirection.isBetween(lower: 90, upper: 135, including: false) {
			windDirectionNotation = "ESE"
		} else if windDirection == 135 {
			windDirectionNotation = "SE"
		} else if windDirection.isBetween(lower: 135, upper: 180, including: false) {
			windDirectionNotation = "SSE"
		} else if windDirection == 180 {
			windDirectionNotation = "S"
		} else if windDirection.isBetween(lower: 180, upper: 225, including: false) {
			windDirectionNotation = "SSW"
		} else if windDirection == 225 {
			windDirectionNotation = "SW"
		} else if windDirection.isBetween(lower: 225, upper: 270, including: false) {
			windDirectionNotation = "WSW"
		} else if windDirection == 270 {
			windDirectionNotation = "W"
		} else if windDirection.isBetween(lower: 270, upper: 315, including: false) {
			windDirectionNotation = "WNW"
		} else if windDirection == 315 {
			windDirectionNotation = "NW"
		} else if windDirection.isBetween(lower: 315, upper: 360, including: false) {
			windDirectionNotation = "NNW"
		} else if windDirection == 360 {
			windDirectionNotation = "N"
		} else {
			windDirectionNotation = "undefined"
		}
		
		var speedUnit = "m/s"
		
		if Store.shared.currentUnitSystem == .imperial {
			speedUnit = "M/s"
		}
		
		wind = windDirectionNotation.appending(" ").appending(windSpeed.twoDecimalString).appending(" ").appending(speedUnit)
	}
}

public struct Forecast {
	public var arrayEntries: [WeatherEntry]
	
	init(json: JSON) {
		let rawEntries = json["list"].arrayValue
		arrayEntries = rawEntries.flatMap({ jsonEntry -> WeatherEntry? in
			switch jsonEntry {
			case .dictionary(let weatherJSON):
				return WeatherEntry(json: weatherJSON)
			default:
				return nil
			}
		})
		
		arrayEntries = arrayEntries.map { (entry) -> WeatherEntry in
			var mutableEntry = entry
			mutableEntry.placeName = json["city"]["name"].stringValue
			return mutableEntry
		}
	}
}
