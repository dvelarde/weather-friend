//
//  URLRepresentable.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

typealias URLRepresentable = String

internal extension URLRepresentable {
	internal func addParameters(_ params: [Parameter]) -> URLRepresentable {
		var queryToAdd = ""
		if params.count > 0 {
			queryToAdd = "?"
		}
		for i in 0 ..< params.count {
			let param = params[i]
			queryToAdd = queryToAdd.appending(param.getKey()).appending("=").appending(param.getValue())
			if i < params.count - 1 {
				queryToAdd = queryToAdd.appending("&")
			}
			
		}
		return self.appending(queryToAdd)
	}
}
