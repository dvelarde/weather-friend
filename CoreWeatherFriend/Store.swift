//
//  Store.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

public final class Store {
	
	public static let shared = Store()
	public let helpURL: String = "https://weather-friend.github.io"
	let baseURL: String = "http://api.openweathermap.org/data/2.5"
	let apiKey = "c6e381d8c7ff98f0fee43775817cf6ad"
	let requestQueue = OperationQueue()
	
	private init() {
		
	}
	
	var savedLocations: [BMLocation] {
		get {
			return UserSettings.savedLocations
		}
		set {
			UserSettings.savedLocations = newValue
		}
	}
	
	public var currentUnitSystem: UnitSystem {
		return UserSettings.currentSystem
	}
	
	public func enqueueOperationWithDependancies(operation: Operation) {
		for dependancy in operation.dependencies {
			enqueueOperationWithDependancies(operation: dependancy)
		}
		requestQueue.addOperation(operation)
	}
	
}
