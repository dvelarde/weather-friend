//
//  Temperature.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

public struct Temperature {
	
	public internal(set) var temperatureValue: Double = 0.0
	
	public var string: String {
		
		let tempNumber = NSNumber(value: temperatureValue)
		let formatter = NumberFormatter.standarFormatter()
		if let tempString = formatter.string(from: tempNumber) {
			let system = Store.shared.currentUnitSystem
			switch system {
			case .imperial:
				return "\(tempString) ℉"
			case .metric:
				return "\(tempString) ℃"
			}
		}
		return ""
	}
	
	public var shortString: String {
		let tempNumber = NSNumber(value: temperatureValue)
		let formatter = NumberFormatter.standarFormatter()
		if let tempString = formatter.string(from: tempNumber) {
			return "\(tempString)°"
		}
		return ""
	}
	
	public init(_ value: Double) {
		temperatureValue = value
	}
}
