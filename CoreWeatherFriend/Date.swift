//
//  Date.swift
//  Weather Friend
//
//  Created by David Velarde on 7/11/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

public extension Date {
	public var shortString: String {
		return DateFormatter.localizedString(from: self, dateStyle: .short, timeStyle: .short)
	}
	
	public var dayName: String {
		let formatter = DateFormatter()
		formatter.dateFormat = "EEEE"
		return formatter.string(from: self).capitalized
	}
	
	public var monthDay: String {
		let formatter = DateFormatter()
		formatter.dateFormat = "MMM dd"
		return formatter.string(from: self).capitalized
	}
	
	public var mediumString: String {
		let formatter = DateFormatter()
		formatter.dateFormat = "EEEE, MMM dd"
		return formatter.string(from: self).capitalized
	}
	
	public var onlyHour: String {
		return DateFormatter.localizedString(from: self, dateStyle: .none, timeStyle: .short)
	}
	
	public var tomorrow: Date {
		let dayInSeconds: Double = 24*60*60
		return self.addingTimeInterval(dayInSeconds)
	}
	
	public func isSameDay(otherDate: Date) -> Bool {
		let toEvaluate = Calendar.current.dateComponents([.day, .month, .year], from: otherDate)
		let today = Calendar.current.dateComponents([.day, .month, .year], from: self)
		
		if toEvaluate.day == today.day && toEvaluate.month == today.month && toEvaluate.year == today.year {
			return true
		}
		return false
	}
	
	public var isToday: Bool {
		return Calendar.current.isDateInToday(self)
	}
}
