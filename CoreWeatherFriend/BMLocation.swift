//
//  BMLocation.swift
//  Weather Friend
//
//  Created by David Velarde on 7/11/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

public class BMLocation: NSObject, NSCoding {
	
	let locationName: String
	let lat: Double
	let lon: Double

	public func encode(with aCoder: NSCoder) {
		aCoder.encode(self.locationName, forKey: "locationName")
		aCoder.encode(lat, forKey: "lat")
		aCoder.encode(lon, forKey: "lon")
	}
	
	public init(name: String, lat: Double, lon: Double) {
		self.locationName = name
		self.lat = lat
		self.lon = lon
	}
	
	public required init?(coder aDecoder: NSCoder) {
		guard let locationName = aDecoder.decodeObject(forKey: "locationName") as? String else {
				return nil
		}
		let lat = aDecoder.decodeDouble(forKey: "lat")
		let lon = aDecoder.decodeDouble(forKey: "lon")
		
		self.locationName = locationName
		self.lat = lat
		self.lon = lon
	}
	
}
