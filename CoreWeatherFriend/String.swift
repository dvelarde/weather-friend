//
//  String.swift
//  Weather Friend
//
//  Created by David Velarde on 7/12/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

extension String {
	
	public var length: Int {
		return characters.count
	}

}
