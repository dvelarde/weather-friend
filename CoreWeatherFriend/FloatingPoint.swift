//
//  FloatingPoint.swift
//  Weather Friend
//
//  Created by David Velarde on 7/12/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

public extension Double {
	public var twoDecimalString: String {
		let number = NSNumber(value: self)
		let formatter = NumberFormatter.standarFormatter()
		formatter.minimumFractionDigits = 2
		return formatter.string(from: number)!
	}
}

public extension FloatingPoint {
	public func isBetween(lower: Self, upper: Self, including: Bool = true) -> Bool {
		if including {
			return (self >= lower && self <= upper)
		} else {
			return (self > lower && self < upper)
		}
	}
}
