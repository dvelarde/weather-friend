//
//  WeatherRequest.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

public extension Store {
	public func getOperationForecastFor(latitude: Double, longitude: Double, completionBlock: @escaping (ErrorValue<Forecast>) -> Void) -> NetworkOperation {
		let url = Store.shared.baseURL.appending("/forecast")
		let params = [
			Parameter("lat",latitude),
			Parameter("lon",longitude),
			Parameter("appid",Store.shared.apiKey),
			Parameter("units", Store.shared.currentUnitSystem.rawValue)
		]
		let operation = NetworkOperation(url, params: params, finishedBlock: { result in
			switch result {
			case .error(let description):
				completionBlock(.error(description: description))
			case .json(let data):
				if data != nil {
					let forecast = Forecast(json: data!)
					completionBlock(.value(value: forecast))
				}
			}
		})
		
		return operation
	}
	
	public func getOperationWeatherFor(latitude: Double, longitude: Double, completionBlock: @escaping (ErrorValue<WeatherEntry>) -> Void) -> NetworkOperation {
		let url = Store.shared.baseURL.appending("/weather")
		let params = [
			Parameter("lat",latitude),
			Parameter("lon",longitude),
			Parameter("appid",Store.shared.apiKey),
			Parameter("units", Store.shared.currentUnitSystem.rawValue)
		]
		let operation = NetworkOperation(url, params: params, finishedBlock: { result in
			switch result {
			case .error(let description):
				completionBlock(.error(description: description))
			case .json(let data):
				if data != nil {
					if let entry = WeatherEntry(json: data!) {
						completionBlock(.value(value: entry))
					} else {
						completionBlock(.error(description: ""))
					}
				}
			}
		})
		return operation
	}
}
