//
//  Enumerators.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

public enum UnitSystem: String {
	case metric
	case imperial
}

public enum ErrorValue<T> {
	case error(description: String)
	case value(value: T)
}

public enum WeatherType {
	case clear(day: Bool)
	case fewClouds(day: Bool)
	case scatteredClouds
	case brokenClouds
	case showerRain
	case rain(day: Bool)
	case thunderStorm
	case snow
	case mist
	case unknown
	
	public static func byCode(_ code: Int, isDay: Bool) -> WeatherType {
		if code.isBetween(lower: 200, upper: 232) {
			return .thunderStorm
		} else if code.isBetween(lower: 300, upper: 321) {
			return .showerRain
		} else if code.isBetween(lower: 500, upper: 531) {
			return .rain(day: isDay)
		} else if code.isBetween(lower: 600, upper: 622) {
			return .snow
		} else if code.isBetween(lower: 701, upper: 781) {
			return .mist
		} else if code == 800 {
			return .clear(day: isDay)
		} else if code == 801 {
			return .fewClouds(day: isDay)
		} else if code == 802 {
			return .scatteredClouds
		} else if code.isBetween(lower: 803, upper: 804) {
			return .brokenClouds
		} else {
			return .unknown
		}
	}
	
	public func getFontText(forceDay: Bool = false) -> String {
		switch self {
		case .clear(let day):
			if forceDay || day {
				return "1"
			} else {
				return "6"
			}
		case .fewClouds(let day):
			if forceDay || day {
				return "A"
			} else {
				return "c"
			}
		case .scatteredClouds:
			return "3"
		case .brokenClouds:
			return "G"
		case .showerRain:
			return "K"
		case .rain(let day):
			if forceDay || day {
				return "F"
			} else {
				return "g"
			}
		case .thunderStorm:
			return "Q"
		case .snow:
			return "I"
		case .mist:
			return "…"
		case .unknown:
			return "/"
		}
	}
}
