//
//  DavidFire.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

public struct Parameter {
	private let key: String
	private let value: String
	
	init(_ key: String, _ value: Any) {
		self.key = key
		self.value = "\(value)"
	}
	
	func getValue() -> String {
		return value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
	}
	func getKey() -> String {
		return key.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
	}
}

internal enum RequestResult {
	case error(description: String)
	case json(value: JSON?)
}

public enum HTTPMethod: String {
	case get
	
	var string: String {
		return self.rawValue.uppercased()
	}
}

public enum EncodingType {
	case json
	case url
}

internal class DavidFire {
	
	static let shared = DavidFire()
	private let session: URLSession!
	private let timeout: TimeInterval
	private init() {
		session = URLSession.shared
		timeout = 60
	}
	
	let requestQueue = OperationQueue()
	
	func request(_ urlString: URLRepresentable, params: [Parameter], httpMethod: HTTPMethod = .get, encoding: EncodingType = .url, completionBlock: @escaping (RequestResult) -> Void) -> URLSessionDataTask? {
		
		var validURL = urlString
		
		if encoding == .url {
			validURL = urlString.addParameters(params)
		}
		
		if let url = URL(string: validURL) {
			
			var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: timeout)
			request.httpMethod = httpMethod.string
			request.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
			//TODO: Generate function to add HTTPBody as Data using params array in case encoding is JSON
			let dataTask = self.session.dataTask(with: request, completionHandler: { (data, response, error) in
				if error != nil {
					completionBlock(.error(description: error!.localizedDescription))
				} else {
					var potentialJSON = [AnyHashable: Any]()
					if data != nil {
						if let json = try? JSONSerialization.jsonObject(with: data!, options: []) as? [AnyHashable: Any],
							json != nil {
							potentialJSON = json!
							let jsonObject = JSONParser.parse(potentialJSON)
							completionBlock(.json(value: jsonObject))
						}
					}
					completionBlock(.json(value: nil))
				}
			})
			
			return dataTask
		}
		return nil
	}
	
}
