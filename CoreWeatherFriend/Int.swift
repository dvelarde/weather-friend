//
//  IntCollectionViewCell.swift
//  Weather Friend
//
//  Created by David Velarde on 7/12/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

public extension Int {
	public func isBetween(lower: Int, upper: Int, including: Bool = true) -> Bool {
		if including {
			return (self >= lower && self <= upper)
		} else {
			return (self > lower && self < upper)
		}
	}
}
