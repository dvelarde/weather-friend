//
//  StringTests.swift
//  Weather Friend
//
//  Created by David Velarde on 7/13/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import XCTest

class StringTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
	func testLength() {
		var string: String! = "Hi everybody!"
		XCTAssert(string.length == 13, "Wrong character count")
		string = ""
		XCTAssert(string.length == 0, "Wrong character count")
	}
		
}
