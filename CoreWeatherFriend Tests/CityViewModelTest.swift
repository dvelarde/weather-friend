//
//  CityViewModelTest.swift
//  Weather Friend
//
//  Created by David Velarde on 7/13/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import XCTest

@testable import CoreWeatherFriend

class CityViewModelTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
	
	func testLocationRecovery() {
		let location = BMLocation(name: "Lima", lat: 10.23, lon: 10.25)
		XCTAssert(location.lat == 10.23, "Wrong latitude on BMLocation")
		XCTAssert(location.lon == 10.25, "Wrong longitude on BMLocation")
		XCTAssert(location.locationName == "Lima", "Wrong name on BMLocation")
		
		let data = NSKeyedArchiver.archivedData(withRootObject: location)
		UserDefaults.standard.set(data, forKey: "DataSaved")
		UserDefaults.standard.synchronize()
		let dataRecovered = UserDefaults.standard.data(forKey: "DataSaved")
		UserDefaults.standard.removeObject(forKey: "DataSaved")
		let locationRecovered = NSKeyedUnarchiver.unarchiveObject(with: dataRecovered!) as! BMLocation
		
		XCTAssert(location.lat == locationRecovered.lat, "Wrong latitude recovery on BMLocation")
		XCTAssert(location.lon == locationRecovered.lon, "Wrong longitude recovery on BMLocation")
		XCTAssert(location.locationName == locationRecovered.locationName, "Wrong name recovery on BMLocation")
	}
	
    func testInitialization() {
		
		let location = BMLocation(name: "Lima", lat: 10.23, lon: 10.25)
		XCTAssert(location.lat == 10.23, "Wrong latitude on BMLocation")
		XCTAssert(location.lon == 10.25, "Wrong longitude on BMLocation")
		XCTAssert(location.locationName == "Lima", "Wrong name on BMLocation")
		
		let vm = CityViewModel(withBMLocation: location)
		XCTAssert(vm.cityName == "Lima", "Wrong name on BMLocation")
		XCTAssert(vm.latitude == 10.23, "Wrong latitude on BMLocation")
		XCTAssert(vm.longitude == 10.25, "Wrong longitude on BMLocation")
		XCTAssert(vm.entry == nil, "Wrong entry on BMLocation")
		
    }
    
}
