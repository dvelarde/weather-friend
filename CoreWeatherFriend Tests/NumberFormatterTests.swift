//
//  NumberFormatterTests.swift
//  Weather Friend
//
//  Created by David Velarde on 7/13/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import XCTest

class NumberFormatterTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
	func testStandarFormatter() {
		let arrayValues = [0,0.1,2,200,20.1,23.43,23.123123123123,-23]
		let valid = ["0","0.1","2","200","20.1","23.4","23.1","-23"]
		for i in 0..<arrayValues.count {
			let value = arrayValues[i]
			let number = NSNumber(value: value)
			let formatter = NumberFormatter.standarFormatter()
			let validString = valid[i]
			XCTAssert(validString == formatter.string(from: number), "Wrong format for Number Formatter")
		}
	}
		
}
