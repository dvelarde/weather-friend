//
//  FloatingPointTest.swift
//  Weather Friend
//
//  Created by David Velarde on 7/13/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import XCTest

class FloatingPointTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
	func testTwoDecimalString() {
		let number = 2.35223123
		XCTAssert(number.twoDecimalString == "2.35", "Wrong two decimal string generation \(number.twoDecimalString)")
	}
		
}
