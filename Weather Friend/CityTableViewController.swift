//
//  CityTableViewController.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import CoreWeatherFriend

class CityTableViewController: UITableViewController {
	
	var cityWeatherEntry: WeatherEntry!
	var cityForecast: Forecast!
	var arrayForecast = [[WeatherEntry]]()
	var selectedIndex = -1 {
		didSet {
			guard let control = tableView else {
				return
			}
			if oldValue == -1 && selectedIndex != -1 {
				control.insertRows(at: [IndexPath(row: 1, section: selectedIndex)], with: .automatic)
			} else if oldValue != -1 && selectedIndex != -1 {
				if oldValue == selectedIndex {
					self.selectedIndex = -1
					control.deleteRows(at: [IndexPath(row: 1, section: oldValue)], with: .automatic)
				} else {
					control.beginUpdates()
					control.deleteRows(at: [IndexPath(row: 1, section: oldValue)], with: .automatic)
					control.insertRows(at: [IndexPath(row: 1, section: selectedIndex)], with: .automatic)
					control.endUpdates()
				}
			}
			if selectedIndex > -1 {
				control.scrollToRow(at: IndexPath(row: 0, section: selectedIndex), at: .top, animated: true)
			}
		}
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()

		if let homeVC = self.getSplitMaster() as? HomeTableViewController {
			self.cityWeatherEntry = homeVC.preLoadedWeatherEntry
			self.cityForecast = homeVC.preLoadedForecast
		}
		
		var dateToAdd = cityForecast.arrayEntries.first!.entryDate
		var arrayDailyForecast = [WeatherEntry]()
		arrayDailyForecast.append(cityWeatherEntry)
		for entry in cityForecast.arrayEntries {
			if !dateToAdd.isSameDay(otherDate: entry.entryDate) {
				dateToAdd = dateToAdd.tomorrow
				arrayForecast.append(arrayDailyForecast)
				arrayDailyForecast = [WeatherEntry]()
			}
			arrayDailyForecast.append(entry)
		}
		arrayForecast.append(arrayDailyForecast)
		
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
		
		tableView.estimatedRowHeight = 70
		
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return arrayForecast.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		if selectedIndex == section {
			return 2
		} else {
			return 1
		}
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		if indexPath.section == 0 {
			let cell = tableView.dequeueReusableCell(withIdentifier: "currentWeatherCell", for: indexPath) as! CityMainTableViewCell
			cell.delegate = self
			cell.setupView(forMainCell: arrayForecast.first!)
			return cell
		} else {
			
			if indexPath.section > 0 && indexPath.row == 0 {
				let entryArray = arrayForecast[indexPath.section]
				let entry = entryArray.first!
				var minTemp: Double = 100000
				var maxTemp: Double = -100000
				for tempEntry in entryArray {
					if tempEntry.minTemp.temperatureValue < minTemp {
						minTemp = tempEntry.minTemp.temperatureValue
					}
					if tempEntry.maxTemp.temperatureValue > maxTemp {
						maxTemp = tempEntry.maxTemp.temperatureValue
					}
				}
				
				let cell = tableView.dequeueReusableCell(withIdentifier: "dailyForecastCell", for: indexPath) as! CityDailyForecastTableViewCell
				
				cell.setupView(info: entry.info, entryDate: entry.entryDate, minTemp: minTemp, maxTemp: maxTemp)
				return cell
			} else if selectedIndex == indexPath.section && indexPath.row == 1 {
				let cell = tableView.dequeueReusableCell(withIdentifier: "forecastWeatherCell", for: indexPath) as! CityMainTableViewCell
				cell.delegate = self
				cell.setupView(forExtraCell: arrayForecast[selectedIndex])
				return cell
			}
		}
		
		return UITableViewCell()
		
    }


    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
	
	override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		if indexPath.row == 0 && indexPath.section == 0 {
			return 400
		} else if indexPath.section == selectedIndex && indexPath.row == 1 {
			return 330
		}
		return UITableViewAutomaticDimension
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if indexPath.section > 0 && indexPath.row == 0 {
			selectedIndex = indexPath.section
		}
	}
	
	override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		if section == 0 && UIDevice.current.userInterfaceIdiom == .pad {
			return 20
		}
		return 0
	}
	
	override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
		if section == 0 && UIDevice.current.userInterfaceIdiom == .pad {
			let view = UIView()
			view.backgroundColor = UIColor(red: 21.0/255.0, green: 145.0/255.0, blue: 1, alpha: 1)
			return view
		}
		return nil
	}
	
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CityTableViewController: CityMainCellDelegate {
	func requireUpdate(cell: CityMainTableViewCell) {
		let contentOffset = self.tableView.contentOffset
		self.tableView.reloadData()
		self.tableView.layoutIfNeeded()
		self.tableView.setContentOffset(contentOffset, animated: false)
	}
}
