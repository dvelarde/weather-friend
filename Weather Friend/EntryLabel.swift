//
//  Label.swift
//  Weather Friend
//
//  Created by David Velarde on 7/12/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

@IBDesignable
public class EntryLabel: UILabel {
	
	@IBInspectable public var topic: String! = "" {
		didSet {
			setupView()
		}
	}
	@IBInspectable public var content: String! = "" {
		didSet {
			setupView()
		}
	}
	
	public func setupView() {
		let fontName = self.font.fontName
		let pointSize = self.font.pointSize
		
		let textToInsert = "\(topic!): \(content!)"
		
		guard
			let regularFont = UIFont(name: fontName, size: pointSize),
			let boldFont = UIFont(name: fontName.appending("-Bold"), size: pointSize)
		else {
			self.text = textToInsert
			return
		}
		
		let attrString = NSMutableAttributedString(string: textToInsert)
		attrString.setAttributes([NSForegroundColorAttributeName : self.textColor], range: NSRange(location: 0, length: textToInsert.length))
		attrString.setAttributes([NSFontAttributeName: regularFont], range: NSRange(location: 0, length: topic.length + 2))
		attrString.setAttributes([NSFontAttributeName: boldFont], range: NSRange(location: topic.length + 2, length: content.length))
		
		attributedText = attrString
	}
	
	override public func prepareForInterfaceBuilder() {
		setupView()
	}
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.setupView()
	}
	
	required public init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
		self.setupView()
	}

}
