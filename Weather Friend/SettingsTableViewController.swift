//
//  SettingsTableViewController.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import CoreWeatherFriend

class SettingsTableViewController: UITableViewController {

	@IBOutlet weak var lblUnitSystem: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

		self.lblUnitSystem.text = Store.shared.currentUnitSystem.rawValue.capitalized
		
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
	
	/*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
	*/

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

	func updateView() {
		self.lblUnitSystem.text = Store.shared.currentUnitSystem.rawValue.capitalized
		self.tableView.reloadData()
	}
	
	// MARK: - Table view delegate
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		tableView.deselectRow(at: indexPath, animated: true)
		if indexPath.row == 0 {
			let controller = UIAlertController(title: "Unit System", message: "Please select your preffered unit system", preferredStyle: .actionSheet)
			controller.addAction(UIAlertAction(title: UnitSystem.metric.rawValue.capitalized, style: .default, handler: { _ in
				DataManager.shared.setUnitSystem(newSystem: .metric)
				self.updateView()
			}))
			controller.addAction(UIAlertAction(title: UnitSystem.imperial.rawValue.capitalized, style: .default, handler: { _ in
					DataManager.shared.setUnitSystem(newSystem: .imperial)
					self.updateView()
			}))
			controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
			
			let rect = tableView.rectForRow(at: indexPath)
			let view = tableView.cellForRow(at: indexPath)
			self.presentActionSheet(controller: controller, sourceView: view, sourceRect: rect)
			
		} else if indexPath.row == 1 {
			let controller = UIAlertController(title: "Warning", message: "Are you sure you want to delete all your bookmarked cities", preferredStyle: .alert)
			controller.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
				DataManager.shared.deleteAllLocations()
				self.presentAlert(title: "Phew", message: "All your cities were deleted successfuly")
			}))
			controller.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
			
			self.present(controller, animated: true, completion: nil)
		}
	}
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
