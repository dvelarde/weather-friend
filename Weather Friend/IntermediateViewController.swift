//
//  IntermediateViewController.swift
//  Weather Friend
//
//  Created by David Velarde on 7/13/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

class IntermediateViewController: UIViewController {

	@IBOutlet weak var lblInfoMessage: UILabel!
	@IBOutlet weak var spiner: UIActivityIndicatorView!
	
	var isLoading = false
	var cityName = ""
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func viewWillAppear(_ animated: Bool) {
		if isLoading {
			spiner.isHidden = false
			spiner.startAnimating()
			lblInfoMessage.text = "Loading \(cityName) forecast"
		}
	}
	
	func setLoadingCity(cityName: String) {
		self.cityName = cityName
		isLoading = true
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
