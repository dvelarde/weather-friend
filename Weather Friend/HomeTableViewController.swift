//
//  HomeTableViewController.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import CoreWeatherFriend
class HomeTableViewController: UITableViewController {

	var arrayVM = [CityViewModel]()
	var preLoadedForecast: Forecast?
	var preLoadedWeatherEntry: WeatherEntry?
	var isLoading = false
	var lastUpdate: Date?
    override func viewDidLoad() {
        super.viewDidLoad()
		self.splitViewController?.preferredDisplayMode = .allVisible
        // Uncomment the following line to preserve selection between presentations
		if UIDevice.current.userInterfaceIdiom == .pad {
			self.clearsSelectionOnViewWillAppear = false
		}

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
		
		//Erase lines that appear on empty tableview
		self.tableView.tableFooterView = UIView()
		self.tableView.estimatedRowHeight = 44
		self.tableView.rowHeight = UITableViewAutomaticDimension
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func viewWillAppear(_ animated: Bool) {
		if UIDevice.current.userInterfaceIdiom == .pad {
			let interVC = self.storyboard?.instantiateViewController(withIdentifier: "intermediate") as! IntermediateViewController
			self.splitViewController?.showDetailViewController(interVC, sender: nil)
		}
		
		let bookmarkedLocations = DataManager.shared.getBMLocations()
		arrayVM = bookmarkedLocations.map({ location -> CityViewModel in
			return CityViewModel(withBMLocation: location)
		})
		
		tableView.reloadData()
		
		if !isLoading {
			
			let finishRefreshingOperation = BlockOperation {
				DispatchQueue.main.async {
					self.lastUpdate = Date()
					self.isLoading = false
					
					self.tableView.reloadData()
				}
			}
			
			isLoading = true
			for i in 0 ..< arrayVM.count {
				var vm = arrayVM[i]
				let operation = Store.shared.getOperationWeatherFor(latitude: vm.latitude, longitude: vm.longitude, completionBlock: { response in
					switch response {
						
					case .value(let value):
						vm.entry = value
						self.arrayVM[i] = vm
					case .error:
						break
					}
					DispatchQueue.main.async {
						self.tableView.reloadData()
					}
				})
				
				finishRefreshingOperation.addDependency(operation)
				
			}
			
			Store.shared.enqueueOperationWithDependancies(operation: finishRefreshingOperation)
			
		}
	}
	@IBAction func onAdd(_ sender: Any) {
		self.splitViewController?.performSegue(withIdentifier: "goToMap", sender: self)
	}

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
		if arrayVM.count > 0 {
			return arrayVM.count
		} else {
			return 1
		}
    }

	
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		
		if arrayVM.count > 0 {
			let cell = tableView.dequeueReusableCell(withIdentifier: "cityWeatherCell", for: indexPath) as! CityWeatherTableViewCell
			
			// Configure the cell...
			
			let vm = arrayVM[indexPath.row]
			
			cell.setupView(placeName: vm.cityName, entry: vm.entry)
			
			
			return cell
		} else {
			let cell = tableView.dequeueReusableCell(withIdentifier: "noCitiesCell")!
			
			return cell
		}
		
		
    }
	

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            DataManager.shared.deleteLocation(index: indexPath.row)
			arrayVM.remove(at: indexPath.row)
			if arrayVM.count > 0 {
				tableView.deleteRows(at: [indexPath], with: .fade)
			} else {
				tableView.reloadRows(at: [indexPath], with: .fade)
			}
        }
    }
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		
		if arrayVM.count == 0 {
			return
		}
		
		if !isLoading {
			isLoading = true
			if let selectedIndexPath = tableView.indexPathForSelectedRow {
				let entry = arrayVM[selectedIndexPath.row]
				let operation = Store.shared.getOperationForecastFor(latitude: entry.latitude, longitude: entry.longitude, completionBlock: { response in
					self.isLoading = false
					DispatchQueue.main.async {
						switch response {
						case .error(let description):
							self.presentAlert(title: "Oops", message: description)
						case .value(let value):
							self.preLoadedWeatherEntry = entry.entry
							self.preLoadedForecast = value
							self.splitViewController?.performSegue(withIdentifier: "goToCityDetail", sender: self)
						}
					}
				})
				if UIDevice.current.userInterfaceIdiom == .pad {
					if let interVC =  self.splitViewController?.viewControllers[1] as? IntermediateViewController {
						interVC.setLoadingCity(cityName: entry.cityName)
					} else {
						let interVC = self.storyboard?.instantiateViewController(withIdentifier: "intermediate") as! IntermediateViewController
						interVC.setLoadingCity(cityName: entry.cityName)
						self.splitViewController?.showDetailViewController(interVC, sender: self)
					}
				}
				Store.shared.enqueueOperationWithDependancies(operation: operation)
				
			}
		}
		
		
	}
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		if isLoading {
			return "Updating weather conditions"
		} else {
			if lastUpdate != nil {
				return "Last Updated on ".appending(lastUpdate!.shortString)
			} else if arrayVM.count > 0 {
				return "There was an error 😢"
			} else {
				return "No cities found"
			}
		}
	}
	
	
    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

	
    // MARK: - Navigation
	
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
		
		
		if segue.destination is CityTableViewController {
			if let selectedIndexPath = tableView.indexPathForSelectedRow {
				if let selectedEntry = arrayVM[selectedIndexPath.row].entry {
					if let vc = segue.destination as? CityTableViewController {
						vc.cityWeatherEntry = selectedEntry
						vc.cityForecast = preLoadedForecast!
					}
				}
			}
		}
		
    }

}
