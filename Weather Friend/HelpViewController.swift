//
//  HelpViewController.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import CoreWeatherFriend

class HelpViewController: UIViewController {

	// MARK: - IBOutlets
	
	enum WebViewState {
		case loading
		case done
		case noContent
	}
	var currentState: WebViewState! {
		didSet {
			switch currentState! {
			case .noContent:
				break
			case .done:
				finishProgress()
			case .loading:
				startProgress()
			}
		}
	}
	
	@IBOutlet weak var webView: UIWebView!
	@IBOutlet weak var progressView: UIProgressView!
	
	var isRunning = false
	var myTimer: Timer!
	var urlReceived: String = ""
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		currentState = .noContent
		
		webView.delegate = self
        // Do any additional setup after loading the view.
		if let url = URL(string: Store.shared.helpURL) {
			let request = URLRequest(url: url)
			webView.loadRequest(request)
			currentState = .loading
		}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

	func startProgress() {
		self.progressView.progress = 0.0
		UIView.animate(withDuration: 0.2, animations: {
			self.progressView.isHidden = false
		})
		self.isRunning = false
		self.myTimer = Timer.scheduledTimer(timeInterval: 0.01, target: self, selector: #selector(timerCallback), userInfo: nil, repeats: true)
	}
	
	func finishProgress() {
		self.isRunning = true
	}
	
	func timerCallback() {
		if self.isRunning {
			UIView.animate(withDuration: 0.2, animations: {
				self.progressView.isHidden = true
			})
			self.myTimer.invalidate()
		} else {
			self.progressView.progress += 0.001
			if self.progressView.progress >= 0.95 {
				self.progressView.progress = 0.95
			}
		}
	}
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HelpViewController: UIWebViewDelegate {
	
	func webViewDidFinishLoad(_ webView: UIWebView) {
		currentState = .done
	}
	func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
		currentState = .done
	}
	func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
		
		if let url = request.url?.absoluteString {
			self.title = url
		}
		return true
	}
}

