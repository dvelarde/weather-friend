//
//  CityDailyForecastTableViewCell.swift
//  Weather Friend
//
//  Created by David Velarde on 7/13/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import CoreWeatherFriend
class CityDailyForecastTableViewCell: UITableViewCell {

	@IBOutlet weak var lblWeatherInfo: UILabel!
	@IBOutlet weak var lblDateDayName: UILabel!
	@IBOutlet weak var lblMinMaxTemp: UILabel!
	@IBOutlet weak var lblDateMonthDay: UILabel!
	
	func setupView(info: WeatherInfo, entryDate: Date, minTemp: Double, maxTemp: Double) {
		lblWeatherInfo.text = info.type.getFontText(forceDay: true)
		lblDateDayName.text = entryDate.dayName
		lblDateMonthDay.text = entryDate.monthDay
		lblMinMaxTemp.text = Temperature(minTemp).shortString.appending("/").appending(Temperature(maxTemp).shortString)
	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
