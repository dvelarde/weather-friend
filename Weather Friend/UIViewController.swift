//
//  AlertHelper.swift
//  Weather Friend
//
//  Created by David Velarde on 7/11/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

public extension UIViewController {
	
	enum ViewControllerType {
		case home
		case settings
		case help
		case none
	}
	
	func getType() -> ViewControllerType {
		if self is HomeTableViewController {
			return .home
		} else if self is SettingsTableViewController {
			return .settings
		} else if self is HelpViewController {
			return .help
		} else{
			return .none
		}
	}
	
	func getSplitMaster() -> UIViewController? {
		if let navController = self.splitViewController?.viewControllers.first as? UINavigationController {
			return navController.viewControllers.first
		}
		return nil
	}
	
	func presentAlert(title: String, message: String) {
		let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
		let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
		controller.addAction(action)
		self.present(controller, animated: true, completion: nil)
	}
	
	func presentActionSheet(controller: UIAlertController, sourceView: UIView? = nil, sourceRect: CGRect) {
		if UIDevice.current.userInterfaceIdiom == .pad {
			
			let popPresenter = controller.popoverPresentationController!
			
			popPresenter.sourceView = sourceView
			popPresenter.sourceRect = sourceRect
			
			self.present(controller, animated: true, completion: nil)
		} else {
			self.present(controller, animated: true, completion: nil)
		}
	}
	
}
