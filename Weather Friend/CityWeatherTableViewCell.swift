//
//  CityWeatherTableViewCell.swift
//  Weather Friend
//
//  Created by David Velarde on 7/10/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import CoreWeatherFriend

class CityWeatherTableViewCell: UITableViewCell {

	@IBOutlet weak var lblCityName: UILabel!
	@IBOutlet weak var lblTemperature: UILabel!
	
	@IBOutlet weak var lblWeatherInfo: UILabel!
	
	func setupView(placeName: String, entry: WeatherEntry?) {
		lblCityName.text = placeName
		if let entry = entry {
			lblTemperature.text = entry.temperature.string
			lblWeatherInfo.text = entry.info.type.getFontText()
		} else {
			lblTemperature.text = "--"
			lblWeatherInfo.text = WeatherType.unknown.getFontText()
		}
	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	override func draw(_ rect: CGRect) {
		let separator = CALayer()
		separator.frame = CGRect(x: 0, y: rect.height - 1, width: rect.width, height: 1)
		separator.backgroundColor = UIColor.lightGray.cgColor
		self.layer.addSublayer(separator)
	}

}
