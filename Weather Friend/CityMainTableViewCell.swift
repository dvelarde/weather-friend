//
//  CityMainTableViewCell.swift
//  Weather Friend
//
//  Created by David Velarde on 7/12/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import CoreWeatherFriend

protocol CityMainCellDelegate: class {
	func requireUpdate(cell: CityMainTableViewCell)
}

class CityMainTableViewCell: UITableViewCell {

	@IBOutlet weak var collectionView: UICollectionView!
	@IBOutlet weak var lblLocation: UILabel!
	@IBOutlet weak var lblWeatherState: UILabel!
	@IBOutlet weak var lblTemperature: UILabel!
	@IBOutlet weak var lblDate: UILabel!
	@IBOutlet weak var lblHumidity: EntryLabel!
	@IBOutlet weak var lblWind: EntryLabel!
	@IBOutlet weak var lblPressure: EntryLabel!
	@IBOutlet weak var lblRainInfo: EntryLabel!
	@IBOutlet weak var lblSnowInfo: EntryLabel!
	@IBOutlet weak var lblCurrentWeather: UILabel!
	
	weak var delegate: CityMainCellDelegate?
	
	var forecast: [WeatherEntry]!
	
	var selectedIndex = 0
	
	func setupView(forExtraCell forecast: [WeatherEntry]) {
		self.forecast = forecast
		
		let entry = self.forecast[selectedIndex]
		
		lblWeatherState.text = entry.info.state.capitalized
		lblTemperature.text = entry.temperature.shortString
		lblDate.text = entry.entryDate.mediumString
		lblHumidity.content = "\(entry.humidity.twoDecimalString) %"
		lblPressure.content = "\(entry.pressure.twoDecimalString) hPa"
		lblWind.content = entry.wind
		lblRainInfo.content = entry.rain.twoDecimalString.appending(" mm")
		lblSnowInfo.content = entry.snow.twoDecimalString.appending(" mm")
		lblCurrentWeather.text = entry.info.type.getFontText()
		
		collectionView.reloadData()
	}
	
	func setupView(forMainCell forecast: [WeatherEntry]) {
		self.forecast = forecast
		
		let entry = self.forecast[selectedIndex]
		
		lblLocation.text = entry.placeName
		lblWeatherState.text = entry.info.state.capitalized
		lblTemperature.text = entry.temperature.shortString
		lblDate.text = entry.entryDate.mediumString
		lblHumidity.content = "\(entry.humidity.twoDecimalString) %"
		lblPressure.content = "\(entry.pressure.twoDecimalString) hPa"
		lblWind.content = entry.wind
		lblRainInfo.content = entry.rain.twoDecimalString.appending(" mm")
		lblSnowInfo.content = entry.snow.twoDecimalString.appending(" mm")
		lblCurrentWeather.text = entry.info.type.getFontText()
		
		collectionView.reloadData()
	}
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

extension CityMainTableViewCell: UICollectionViewDataSource {
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		if forecast != nil {
			return 1
		} else {
			return 0
		}
	}
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return forecast.count
	}
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "weatherTimeCell", for: indexPath) as! WeatherTimeCollectionViewCell
		
		let currentEntry = forecast[indexPath.item]
		
		
		cell.setupView(type: currentEntry.info.type, time: currentEntry.entryDate.onlyHour, temperature: currentEntry.temperature, selected: indexPath.item == selectedIndex)
		
		return cell
		
	}
}

extension CityMainTableViewCell: UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		selectedIndex = indexPath.item
		if let delegate = delegate {
			delegate.requireUpdate(cell: self)
		}
	}
}
