//
//  CustomSplitViewController.swift
//  Weather Friend
//
//  Created by David Velarde on 7/13/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit

class CustomSplitViewController: UISplitViewController {

	
	override var displayMode: UISplitViewControllerDisplayMode {
		return .allVisible
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		self.delegate = self
		
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func viewWillAppear(_ animated: Bool) {
		for child in self.viewControllers {
			child.viewWillAppear(animated)
		}
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CustomSplitViewController: UISplitViewControllerDelegate {
	func splitViewController(_ splitViewController: UISplitViewController, collapseSecondary secondaryViewController: UIViewController, onto primaryViewController: UIViewController) -> Bool {
		return true
	}
}
