//
//  WeatherTimeCollectionViewCell.swift
//  Weather Friend
//
//  Created by David Velarde on 7/12/17.
//  Copyright © 2017 David Velarde. All rights reserved.
//

import UIKit
import CoreWeatherFriend

public class WeatherTimeCollectionViewCell: UICollectionViewCell {
	@IBOutlet weak var lblWeatherType: UILabel!
	@IBOutlet weak var lblWeatherTime: UILabel!
	@IBOutlet weak var lblTemperature: UILabel!
	
	func setupView(type: WeatherType, time: String, temperature: Temperature, selected: Bool = false) {
		lblWeatherType.text = type.getFontText()
		lblWeatherTime.text = time
		lblTemperature.text = temperature.shortString
		self.layer.borderColor = UIColor(red: 21.0/255.0, green: 145.0/255.0, blue: 1, alpha: 1).cgColor
		if selected {
			self.layer.borderWidth = 2
		} else {
			self.layer.borderWidth = 0
		}
	}
}
